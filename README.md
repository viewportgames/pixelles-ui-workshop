# Pixelles UI Workshop

Layout Groups are simpler than they seem. If you take a look at how these examples are structured in the Unity scene, you'll notice I never use Child Force Expand, and almost always have Control Child Size checked. Get into the habit of making that your default Layout Group setting, and experimenting from there. In almost all cases, every object in a UI structure that has children should have a Horizontal or Vertical Layout Group. Example 4 demonstrates some basic nesting, where it's Layout Groups all the way down from the main window to buttons in the footer.

A note on Resizing Text in Example 6: The method I use to dynamically adjust the font size of Text Mesh Pro text involves swapping Style Sheets in TMPro_Settings. There are likely much better ways to do this. However if you want to enable this, you must first set a Text Mesh Pro property to public in the TMPro source. See comments in ResizeText.cs.

**LAYOUT ELEMENT** ([Manual](https://docs.unity3d.com/Manual/script-LayoutElement.html))

*  **Ignore Layout:** Remove/detach Layout Element from the Layout Group, letting you freely set its transform properties. Useful for stretched images you want to serve as a panel's background image (see Example 2 in project).
*  **Min Width/Height:** The minimum width/height this Layout Element should have (if unchecked, default is 0).
*  **Preferred Width/Height:** Override a Layout Element's default preferred size (To find the default preferred size, check Control Child Size on the parent Layout Group and see what the Layout Element's transform size is set to). The transform size will be capped to the parent Layout Group's maximum size. 
*  **Flexible Width/Height:** This is basically equivalent to a Layout Group's "Child Force Expand," but gives much more control over that expansion and in most cases should be used instead of Child Force Expand. It will increase the Layout Element's size to fit all available space. Setting the value between 0 and 1 controls the proportion of the available space to take up, but in most cases you will set this to 1 and fine tune the size with the Preferred Size property. Notice how I use Flexible Width in Example 2, and for spacing in the header of Example 4.

**HORIZONTAL LAYOUT GROUP** ([Manual](https://docs.unity3d.com/Manual/script-HorizontalLayoutGroup.html))

Along with Vertical Layout Group, this is the fundamental building block for creating UI structures.

*  **Control Child Size:** This tells children to automatically set the width/height on their transform to their preferred size. It is a good idea to turn this on by default when adding a new Layout Group component.
*  **Use Child Scale:** Take the scale of child tranforms into account when calculating size. I don't recommend ever using Scale on UI transforms, as it's better to adjust Text objects through their Font Size and images through their Layout Element's Preferred Size.
*  **Child Force Expand:** Tell all children to expand to take up available space. Basically equivalent to checking Flexible Width/Height on every child's Layout Element, but with no control over which children are effected (they all are), or the relative proportion they should expand to (all are set to 1). I recommend avoiding this and use Flexible Width/Height on children instead. For an example as to why it's a bad idea to use this, try checking *Child Force Expand - Width* on *Panel - Menu* in Example 2, and note what happens to the square logo image.

**VERTICAL LAYOUT GROUP** ([Manual](https://docs.unity3d.com/Manual/script-VerticalLayoutGroup.html))

Identical to Horizontal Layout Group, but children are arranged vertically instead of horizontally.

**CONTENT SIZE FITTER** ([Manual](https://docs.unity3d.com/Manual/script-ContentSizeFitter.html))

The Content Size Fitter is generally placed on the top level of the UI structure you've created, to adjust its size based on the sizes of all of its children and nested children.

*  **Unconstrained:** You manually set the transform's width/height.
*  **Min Size:**  Width/height based on minimum size of all children taken as a whole. (ie. if one child has a width of 20 and another has a width of 40, Horizontal Fit: Min Size will set the transform's width to 40.) 
*  **Preferred Size:** Width/height based on preferred size of all children taken as a whole.

**GRID LAYOUT GROUP** ([Manual](https://docs.unity3d.com/Manual/script-GridLayoutGroup.html))

Not useful for building complex UI structures, as the sizes of its children (cell size) are rigidly defined to always be of equal width and height. Unity documentation does an ok job explaining how it works.

**SPACING**

The Layout Group's Spacing property will apply the same value between all children elements. In order to add additional spacing, add a blank GameObject with a Layout Element to the Layout Group, and control the amount of spacing with Preferred Width/Height.

**FLEXIBLE SCROLLVIEWS**

See Example 5. This is one of the rare cases where a Content Size Fitter is placed within a UI structure, as well as at the very top level. Place a Layout Element on the *Scroll View* with the Preferred or Flexible options you want. Leave the *Viewport* how it is. Add a Content Size Fitter to *Content*, with appropriate Layout Groups.

**MAXIMIMUM WIDTH/HEIGHT**

See Example 3. Create a GameObject with Content Size Fitter and Layout Group. If you want set a maximum width, set Horizontal Fit Unconstrained. For a maximum height, set Vertical Fit Unconstrained. The value you enter in the transform's width and/or height will be the maximum size. Check Control Child Size on the Layout Group. Now make your UI elements children of this object with their standard layout group/element settings. 