﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using PixellesUIWorkshop;

namespace PixellesUIWorkshop{
  public class ExampleScene : MonoBehaviour{

    [SerializeField] private TextMeshProUGUI _cashText;
    private Coroutine cashCountCoroutine;

    private void Start(){
      ResetCashCountCoroutine();
    }

    public void ResetCashCountCoroutine(){
      if(cashCountCoroutine != null){
        StopCoroutine(cashCountCoroutine);
      }
      cashCountCoroutine = StartCoroutine(CashCountCoroutine());
    }

    private IEnumerator CashCountCoroutine(){
      float value = 0f;
      while(true){
        value = value + Time.deltaTime * 100f;

        if(value > 250f){
          value = 0f;
        }

        _cashText.text = "<style=\"Dynamic\">" + "$" + value.ToString("0") + "</style>";
        
        yield return 0;
      }
    }
  }
}
