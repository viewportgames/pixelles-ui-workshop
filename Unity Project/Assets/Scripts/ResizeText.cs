﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using PixellesUIWorkshop;

namespace PixellesUIWorkshop{
  public class ResizeText : MonoBehaviour{

    [SerializeField] private Canvas _canvas;
    [SerializeField] private TMP_StyleSheet[] _styleSheetArray;

    private void Awake(){
      SetTextSize(2);
    }

    public void OnButtonPressed(int value){
      SetTextSize(value);
    }

    private void SetTextSize(int intValue){
      //You must first make TMP_Settings.defaultStyleSheet public in the Text Mesh Pro source before uncommenting these two lines.
//     TMP_Settings.defaultStyleSheet = _styleSheetArray[intValue];
//     TMP_StyleSheet.UpdateStyleSheet();

      /* 
      We need to rebuild every Layout Group affected by the new text size after changing the Style Sheet
      These do not work:
        Canvas.ForceUpdateCanvases();
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)_canvas.gameObject.transform);

      LayoutRebuilder.ForceRebuildLayoutImmediate DOES work if you use it on each top level Layout Group,
      but if you don't want to keep track of them all you can just toggle the Canvas game object ¯\_(ツ)_/¯
      */
      
      _canvas.gameObject.SetActive(false);
      _canvas.gameObject.SetActive(true);
    }
  }
}
