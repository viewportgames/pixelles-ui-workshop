﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using PixellesUIWorkshop;

namespace PixellesUIWorkshop{
  public class ResizeCanvas : MonoBehaviour{
    [SerializeField] private CanvasScaler _canvasScaler;

    public void OnButtonPressed(float value){
      SetCanvasScale(value);
    }

    private void SetCanvasScale(float value){
      _canvasScaler.scaleFactor += value;
    }
  }
}
